<?php

namespace Database\Seeders;
use DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UsersTableData extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
          
           [ 'name'=>'user1',
           'email'=>'user1@gmail.com',
           'password'=>bcrypt('12345678'),
           'is_admin'=>"0"
        ],
        [ 'name'=>'admin',
           'email'=>'admin@gmail.com',
           'password'=>bcrypt('12345678'),
           'is_admin'=>"1"
        ],
          [ 
          'name'=>'user2',
          'email'=>'user2@gmail.com',
          'password'=>bcrypt('12345678'),
          'is_admin'=>"0"
          ],
          [ 'name'=>'user3',
          'email'=>'user3@gmail.com',
          'password'=>bcrypt('12345678'),
          'is_admin'=>"0"
        ]
        ]);
    }
}

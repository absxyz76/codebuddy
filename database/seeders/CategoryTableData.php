<?php

namespace Database\Seeders;
use DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CategoryTableData extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
          
         [ 
            'category_name'=>'start',
            'parent_id'   =>null,
         ],
         [ 
            'category_name'=>'one',
            'parent_id'   =>1,
         ],
         [ 
            'category_name'=>'three',
            'parent_id'   =>null,
         ],
         [ 
            'category_name'=>'four',
            'parent_id'   =>3,
         ],
         [ 
            'category_name'=>'two',
            'parent_id'   =>1,
         ],
         [ 
            'category_name'=>'five',
            'parent_id'   =>4,
         ],
         ]);
    }
}

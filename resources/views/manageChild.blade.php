<!-- Display a category and its children recursively -->
<li class="list-group-item">
    {{ $category->category_name }}
    @method('delete')
    <a href="{{route('deletedata',$category->id)}}" class="btn btn-danger btn-sm" title="Delete" data-toggle="tooltip" style="float: right;margin-left:10px">Delete</a>&nbsp
    <a class="btn btn-primary btn-sm" title="Delete" href="{{route('updateData',$category->id)}}" style="float: right">Edit</a>&nbsp
    
    @if($category->children->count() > 0)
        <ul class="list-group active">
            @foreach($category->children as $child)
                @include('manageChild', ['category' => $child])
            @endforeach
        </ul>
    @endif
</li>


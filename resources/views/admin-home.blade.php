@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><b>Admin</b>&nbsp{{ __('Dashboard') }} <a href="{{route('category')}}" class="btn btn-success" type="btn" style="float: right">Category</a></div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <h3>Welcome to Admin Dashboard</h3>
                    <table class="table table-striped">
                        <thead>
                            <tr class="">
                                <th scope="row">#</th>
                                <th >Name</th>
                                <th colspan="2">Email</th>
                                <th >Dashboard</th>
                              </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i=1
                            ?>
                            @foreach($user as $udata )
                          <tr class="">
                            <th scope="row">{{$i++}}</th>
                            <td >{{$udata->name}}</td>
                            <td colspan="2">{{$udata->email}}</td>
                            <td><a href="{{route('user.dashboard',$udata->id)}}">Dashboard</a></td>
                            
                          </tr>
                          @endforeach
                        </tbody>
                      </table>
                     
                    {{-- {{ __('You are logged in!') }} --}}
                </div>
            </div>
            <!-- Display the category tree -->
            <hr>
            <div class="card">
                <h3>Category</h3>
                <ul class="list-group active">
                    @foreach($alldata as $category)
                        @include('manageChild', ['category' => $category])
                    @endforeach
                </ul>
            </div>
            
            @if (session('success'))
            <div class="alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        @endif
        @if (session('danger'))
        <div class="alert alert-danger" role="alert">
            {{ session('danger') }}
        </div>
    @endif
        </div>
       
    </div>
</div>
@endsection

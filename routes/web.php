<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/admin/home', [App\Http\Controllers\HomeController::class, 'adminHome'])->name('admin.home')->middleware('is_admin');
Route::get('/home/{id}', [App\Http\Controllers\HomeController::class, 'userDash'])->name('user.dashboard');
Route::get('/category', [App\Http\Controllers\HomeController::class, 'categoryView'])->name('category');
Route::post('categoryAdd', [App\Http\Controllers\HomeController::class, 'categoryAdd'])->name('categoryAdd');
Route::get('/deletedata/{id}', [App\Http\Controllers\HomeController::class, 'delete'])->name('deletedata');
Route::get('/categoryUpdate/{id}', [App\Http\Controllers\HomeController::class, 'update'])->name('updateData');
Route::put('updateSave/{id}', [App\Http\Controllers\HomeController::class, 'updateSave'])->name('updateSave');
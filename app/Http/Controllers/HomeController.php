<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\user;
use App\Models\Category;
use App\Facades\Tree;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
    public function adminHome(){
        $user = DB::table('users')
        ->where('is_admin','=',0)
        ->get();
        $categories = new Category();
        $alldata=$categories->tree();
        
        return view('admin-home',compact('user','alldata'));
    }
    public function userDash($id){
        $user = DB::table('users')
        ->where('id','=',$id)
        ->get(); 
        $uname=$user[0]->name;
       return view('home',compact('uname'));  
    }
    public function categoryView(){
        $categories = DB::table('categories')->get();
        
        return view('category',compact('categories'));
    }
    public function categoryAdd(Request $req){
        $validated = $req->validate([
            'category_name' => 'required|unique:categories',
        ]);
        $category = new Category();
        $category->category_name = $req->category_name;
        $category->parent_id = $req->parent_id;
        $category->save();
       
        return redirect()->route('admin.home')->with('success','Category Added successfully!');
    }
    
    public function delete($id,Request $req)
    {
        $category=Category::findorfail($id);
        $category->delete();

        return redirect()->route('admin.home')->with('danger', 'Category deleted successfully.');
    }
    public function update ($id){
        $Category=Category::findorfail($id);
    
       return view('categoryUpdate', compact('Category'));   
    }
    function updateSave($id,Request $req){
        $validated = $req->validate([
            'category_name' => 'required',
        ]);
        $category=Category::find($id);
        $category->category_name = $req->category_name;
        $category->save();
        return redirect()->route('admin.home')->with('success','Category updated successfully!');
            
    }
}
